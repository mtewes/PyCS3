Citing PyCS3 in a publication
=============================

To cite PyCS3, we suggest both of the following papers:

* Tewes et al. 2013

  - `COSMOGRAIL XI: Techniques for time delay measurement in presence of microlensing, A&A 553 A120 <http://dx.doi.org/10.1051/0004-6361/201220123>`_.
  - This paper describes the curve-shifting algorithms of PyCS. 

* Bonvin et al. 2016

  - `COSMOGRAIL XV: Assessing the achievability and precision of time-delay measurements, A&A 585 A88 <http://dx.doi.org/10.1051/0004-6361/201526704>`_.
  - This paper demonstrates the performance of PyCS on synthetic data from the first Time Delay Challenge.

If you make use of the automated time-delay measurement pipeline, please cite :

* Millon et al. 2020

  - `COSMOGRAIL XIX: Time delays in 18 strongly lensed quasars from 15 years of optical monitoring <https://arxiv.org/abs/2002.05736>`_.
  - This paper presents the automated version of PyCS and apply this technique on large monitoring data set.




.. note:: Please use the URL ``http://www.cosmograil.org`` when indicating a website for PyCS3. While the hosting of the code might change, we will try hard to keep this URL valid for the years to come.